#!/bin/bash
############################################################################
#
# Usage: stackbuild.sh
#
# Creates a stack with openscoring and nginx depnding on the configuartion
# passed on the yaml file.
#
# Prerequisites:
# -Install Docker 17.06.2-ce or greater than this.
# -PIP should be installed and configured
# -Setup GIT
# -Setup GIT LFS if its not configured previously.
# -Setup Sysylog, navigate to etc/rsyslog.conf
#      -Edit the file:
#      -Uncomment the lines to activate UDP and TCP syslog reception.
#      -Add at the last of the script
#           $template TmplAuth, 
#           /var/log/%HOSTNAME%/%PROGRAMNAME%.log
#
############################################################################

echo "-----------------------Openscoring Realtime Stack:1.0----------------------" 
sudo pip install shyaml

sudo apt-get install moreutils
containers=$(cat configuration.yml|shyaml get-value general.no_of_parallel_containers)
nginx_name=$(cat configuration.yml|shyaml get-value nginx.container_name)
nginx_image_name=$(cat configuration.yml|shyaml get-value nginx.image) 
nginx_port=$(cat configuration.yml|shyaml get-value nginx.ports) 
openscoring_name=$(cat configuration.yml|shyaml get-value openscoring.container_name) 
openscoring_image_name=$(cat configuration.yml|shyaml get-value openscoring.image)
#pmml_name=$(cat configuration.yml|shyaml get-value general.pmmlname)
#pmml_loc=$(cat configuration.yml|shyaml get-value general.pmmlloc)
network_name=$(cat configuration.yml|shyaml get-value general.network)
niprange=$(cat configuration.yml|shyaml get-value nginx.iprange)
nip=$(cat configuration.yml|shyaml get-value nginx.nip)
domn=$(cat configuration.yml|shyaml get-value general.domn)
#progn=$(cat configuration.yml|shyaml get-value general.progn)

#export HOSTNAME=$hostn
#export PROGRAMNAME=$progn

if [ ! -d "openscoring_rt_setup" ]; then
mkdir openscoring_rt_setup;
fi

cd openscoring_rt_setup

if [ ! -d "nginx" ]; then
git lfs clone https://gitlab.com/avinandan/nginx.git;
fi

if [ ! -d "openscoring" ]; then
git lfs clone https://gitlab.com/avinandan/openscoring.git;
fi

cd nginx
ct=$(sudo docker image ls|grep -w nginx_vanila|wc -l)
if [ "$ct" -eq 0 ]; then
cat nginx_vanila.tar | sudo docker load 
fi
echo "-----------------Nginx Vanila Image is Created------------------"
sed -i 's/@imagename/openscoring/g' nginx.conf


for number in $(eval echo "{1..$(expr $containers - 1)}")
do
sed -i '/server '$openscoring_name$number':8080/a   server '$openscoring_name$(expr $number + 1)':8080;' nginx.conf
#echo server openscoring$(expr $number + 1):8080
done
echo "----------------Nginx is configured------------------------"
sudo docker build -t $nginx_image_name .

echo "----------------Configured Nginx Image is built-----------------"
cd ../openscoring
ct1=$(sudo docker image ls|grep -w openscoring_vanila|wc -l)
if [ "$ct1" -eq 0 ]; then
zcat openscoring_vanila.tar.gz | sudo docker load
fi

echo "----------------Openscoring Image is built---------------------"
#sed -i 's/@ipaddress/172.21.0.'$(expr $containers + 3)'/g' application.conf
sed -i 's/@ipaddress/'$nip'/g' application.conf
sudo docker build -t $openscoring_image_name .

echo "----------------Configured Openscoring Image is Created------------------"
nt=$(sudo docker network ls -f name=$network_name|wc -l)
if [ "$nt" -eq 1 ]; then
sudo docker network create --subnet=$niprange $network_name;
fi

echo "----------------Network is Created-------------------------"
#echo "sudo docker run -d --net" $network_name "-p" $nginx_port "--name" $nginx_name $nginx_image_name


for number in $(eval echo "{1..$containers}")
do
  sudo docker run -d --net $network_name -e JAVA_OPTIONS='-Xmx6g' --log-driver syslog --log-opt syslog-address=udp://localhost:514 --name $openscoring_name$number $openscoring_image_name

echo "----------------$openscoring_name$number container is created-------------------"
  #echo openscoring$number
done

sudo docker run -d --net $network_name --ip $nip -p $nginx_port --name $nginx_name $nginx_image_name
#echo "sudo docker run --net" $network_name "-d -p" $nginx_port "--name" $nginx_name $nginx_image_name

echo "----------------$nginx_name Nginx load balancer container is created-------------------"
#ipaddress=$(hostname -I|awk '{print $1;}')

#curl -X PUT --data-binary @$pmml_loc/$pmml_name.pmml -H "Content-type: text/xml" http://$ipaddress:8080/openscoring/model/$pmml_name
#cat ../../configuration.yml |sudo docker run -i --rm jlordiales/jyparser set ".general.ip" \"$ipaddress\"|sponge ../../configuration.yml

echo "Domain Name: "$domn
#echo "Hosted model address: "http://$ipaddress:8080/openscoring/model/$pmml_name